({
	initialize: function (component, event, helper) {
		var contextData = component.get("v.contextData")
		if (contextData) {
			component.set("v.fieldLabels", contextData.fieldLabels);
			component.set("v.pickListValues", contextData.pickListValues);
			component.set("v.objectLabels", contextData.objectLabels);
			if (!contextData.accountRecordTypeOptions || contextData.accountRecordTypeOptions.length === 0) {
				component.set("v.showAccountCreationForm", true);
        	} else {
				component.set("v.accountRecordTypeOptions", contextData.accountRecordTypeOptions);
				component.set("v.selectedAccountRecordType", contextData.accountRecordTypeOptions[0].value);
			}
		}
  	},
	
	handleSubmit : function(component, event, helper) {
		var params = event.getParams();
		var accountFormData = params.fields;
		
		for (var fieldName in accountFormData) {
			if (accountFormData[fieldName] == null) {
				delete accountFormData[fieldName];
			}
		}
		var newAccount = Object.assign({ 'sobjectType': 'Account' }, accountFormData);

		var createNewAccount = component.get("c.createNewAccount");
		createNewAccount.setParams({
			account: newAccount
		});
		createNewAccount.setCallback(this, function (response) {
			if (response.getState() === "SUCCESS") {
				var accountId = response.getReturnValue();
				helper.toSelectAccountPage(component, event, helper);
				helper.showSuccessMessage(component, $A.get("$Label.c.PaymentLinkingToolLightning_Title_AccountCreated"), 
					$A.get("$Label.c.PaymentLinkingToolLightning_Record_ID") + ': ' + accountId);
			}
			else {
				var errors = response.getError();
				var errorMessage;
				if (errors[0] && errors[0].message) {
					errorMessage = errors[0].message;
				}
				helper.showErrorMessage(component, $A.get("$Label.c.ErrorMessage_SaveError"), errorMessage);
			}
		});
		$A.enqueueAction(createNewAccount);
		event.preventDefault();
	},

	handleCancel : function(component, event, helper) {
		helper.toSelectAccountPage(component, event, helper);
	},

	nextStep: function (component, event, helper) {
		component.set("v.showAccountCreationForm", true);
	},

  	doCancel: function (component, event, helper) {
		helper.toSelectAccountPage(component, event, helper);
	}
})