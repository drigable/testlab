({
	searchAllocations: function (component) {
		component.set("v.inSearching", true);
		component.set("v.allocationsInOfSearch", false);
		var contextData = component.get("v.contextData");
		// reset the selection state before each allocations searching
		component.set("v.allocatePaymentSelectionState", {
			selectedInvoicesCount: 0,
			selectedAgreementsCount: 0,
			selectedFundsCount: 0,
			selectedProjectsCount: 0,
			selectedCampaignsCount: 0
		});
		var getWrappedAllocations = component.get("c.getWrappedAllocations");
		var searchCriteria = component.get("v.searchCriteria");
		searchCriteria.isParishUnion = contextData.isParishUnion;
		getWrappedAllocations.setParams({
			searchCriteria: searchCriteria,
			selectedPayment: contextData.selectedPayment,
			selectedAccount: contextData.selectedAccount
		});
		var OUT_OF_SEARCH = 'OUT_OF_SEARCH';
		var wrappedInvoiceAllocations = component.get("v.wrappedInvoiceAllocations");
		var wrappedAgreementAllocations = component.get("v.wrappedAgreementAllocations");
		var wrappedFundAllocations = component.get("v.wrappedFundAllocations");
		var wrappedProjectAllocations = component.get("v.wrappedProjectAllocations");
		var wrappedCampaignAllocations = component.get("v.wrappedCampaignAllocations");
		wrappedInvoiceAllocations = [OUT_OF_SEARCH];
		wrappedAgreementAllocations = [OUT_OF_SEARCH];
		wrappedFundAllocations = [OUT_OF_SEARCH];
		wrappedProjectAllocations = [OUT_OF_SEARCH];
		wrappedCampaignAllocations = [OUT_OF_SEARCH];
		getWrappedAllocations.setCallback(this, function (response) {
			if (response.getState() === "SUCCESS") {
				component.set("v.inSearching", false);
				component.set("v.activeAccordionSections", ['section-1','section-2','section-3','section-4','section-5']);
				var allocationWrappersResponse = response.getReturnValue();
				var allocationsInOfSearch = false;
				if (allocationWrappersResponse.wrappedInvoiceAllocations) {
					allocationsInOfSearch = true;
					wrappedInvoiceAllocations = allocationWrappersResponse.wrappedInvoiceAllocations.records;
                    component.set("v.hasMoreInvoices", allocationWrappersResponse.wrappedInvoiceAllocations.hasMore);
				}
				if (allocationWrappersResponse.wrappedAgreementAllocations) {
					if (!allocationsInOfSearch) allocationsInOfSearch = true;
					wrappedAgreementAllocations = allocationWrappersResponse.wrappedAgreementAllocations.records;
                    component.set("v.hasMoreAgreements", allocationWrappersResponse.wrappedAgreementAllocations.hasMore);
				}
				if (allocationWrappersResponse.wrappedFundAllocations) {
					if (!allocationsInOfSearch) allocationsInOfSearch = true;
					wrappedFundAllocations = allocationWrappersResponse.wrappedFundAllocations.records;
                    component.set("v.hasMoreFunds", allocationWrappersResponse.wrappedFundAllocations.hasMore);
				}
				if (allocationWrappersResponse.wrappedProjectAllocations) {
					if (!allocationsInOfSearch) allocationsInOfSearch = true;
					wrappedProjectAllocations = allocationWrappersResponse.wrappedProjectAllocations.records;
                    component.set("v.hasMoreProjects", allocationWrappersResponse.wrappedProjectAllocations.hasMore);
				}
				if (allocationWrappersResponse.wrappedCampaignAllocations) {
					if (!allocationsInOfSearch) allocationsInOfSearch = true;
					wrappedCampaignAllocations = allocationWrappersResponse.wrappedCampaignAllocations.records;
                    component.set("v.hasMoreCampaigns", allocationWrappersResponse.wrappedCampaignAllocations.hasMore);
				}
				component.set("v.allocationsInOfSearch", allocationsInOfSearch);
				component.set("v.wrappedInvoiceAllocations", wrappedInvoiceAllocations);
				component.set("v.wrappedAgreementAllocations", wrappedAgreementAllocations);
				component.set("v.wrappedFundAllocations", wrappedFundAllocations);
				component.set("v.wrappedProjectAllocations", wrappedProjectAllocations);
				component.set("v.wrappedCampaignAllocations", wrappedCampaignAllocations);
			}
			else {
				component.set("v.inSearching", false);
				var errors = response.getError();
				var errorMessage;
				if (errors[0] && errors[0].message) {
					errorMessage = errors[0].message;
				}
				this.showErrorMessage(component, $A.get("$Label.c.PaymentLinkingToolLightning_Error_ErrorOccurredOnItemRetrieval"), errorMessage);
			}
		});
		window.setTimeout($A.getCallback(function() {
			$A.enqueueAction(getWrappedAllocations);
        }), 100);
	},

	createPaymentAllocations: function (component, payment, account, selectedWrappersQuantity, allocationNameToWrappedAllocations, createTransferPayments) {
		var createPaymentAllocations = component.get("c.createPaymentAllocations");
		createPaymentAllocations.setParams({
			payment: payment,
			account: account,
			allocationNameToWrappedAllocations: allocationNameToWrappedAllocations,
			selectedWrappersQuantity: selectedWrappersQuantity
		});

		createPaymentAllocations.setCallback(this, function (response) {
			if (response.getState() === "SUCCESS") {
				if (createTransferPayments) {
					this.createTransferPayments(component, payment.Id, 'inSaving');
				}
				else {
					component.set("v.inSaving", false);
					this.switchCurrentComponent(component, "c:PaymentLinkingToolSelectPayment", true, true);
					this.showSuccessMessage(component, null, $A.get("$Label.c.Message_SaveSuccessful"));
				}
			}
			else {
				this.processErrorResponse(component, 'inSaving', response);
			}
		});
		$A.enqueueAction(createPaymentAllocations);
	},
	
	createTransferPayments: function(component, paymentId, actionStatusAttributeName) {
		var createTransferPayments = component.get("c.createTransferPayments");
		createTransferPayments.setParams({
			paymentId: paymentId
		});
		createTransferPayments.setCallback(this, function (response) {
			if (response.getState() === "SUCCESS") {
				component.set("v.inSaving", false);
				this.switchCurrentComponent(component, "c:PaymentLinkingToolSelectPayment", true, true);
				this.showSuccessMessage(component, null, $A.get("$Label.c.Message_SaveSuccessful"));
			}
			else {
				this.processErrorResponse(component, actionStatusAttributeName, response);
			}
		});
		$A.enqueueAction(createTransferPayments);
	},

	processErrorResponse: function(component, actionStatusAttributeName, response) {
		if (actionStatusAttributeName)
			component.set('v.' + actionStatusAttributeName, false);
		
		var errors = response.getError();
		var errorMessage;
		if (errors[0] && errors[0].message) {
			errorMessage = errors[0].message;
		}
		if (errorMessage && this.isJsonString(errorMessage)) {
			var errorData = JSON.parse(errorMessage);
			if (errorData.title && errorData.errorMessage) {
				if (errorData.wrapperType && errorData.tableRowIndex) {
					var objectLabels = component.get("v.contextData").objectLabels;
					this.showErrorMessage(component, errorData.title,
						objectLabels[errorData.wrapperType].labelPlural
						+ ', ' + $A.get("$Label.c.PaymentLinkingToolLightning_Error_AllocationsToCreateMessagePart") + ' '
						+ errorData.tableRowIndex + ': '
						+ errorData.errorMessage
					);
					return;
				} else {
					this.showErrorMessage(component, errorData.title, errorData.errorMessage);
					return;
				}
			}
		}
		this.showErrorMessage(component, $A.get("$Label.c.ErrorMessage_SaveError"), errorMessage);
    },
	
	checkTotalAmountAgainstPaymentAmount: function (component, payment, allocationNameToWrappedAllocations, wrappedSelectedAllocationsMap, selectedWrappers) {
		var check = true;
		var totalAmount = 0;
		var selectedAllocationsWithNoAmount = 0;

		for (var allocationName in allocationNameToWrappedAllocations) {
			wrappedSelectedAllocationsMap[allocationName] = allocationNameToWrappedAllocations[allocationName].filter(function (wrapper) {
				var amount = wrapper.allocation.Amount__c;
				amount = !isNaN(amount) ? parseFloat(amount) : '';

				if (wrapper.isSelected && !(amount > 0))
					selectedAllocationsWithNoAmount++;

				// should the checkbox be selected or it is enough that there is an amount, but excluding invoices
				if (wrapper.isSelected || (wrapper.wrapperType !== 'Invoice__c' && amount > 0)) {
					if (!wrapper.isSelected) {
						wrapper.isSelected = true;
						this.updateAllocatePaymentSelectionState(component, wrapper.wrapperType, true);
					}
					totalAmount += (amount != null ? amount : 0);
					selectedWrappers.push(wrapper);
					return wrapper;
				}
			}, this);
		}

		// If user selects 1 record but does not enter amount, the full amount of payment is copied to the allocation
		if (selectedWrappers.length === 1 && selectedWrappers[0].allocation.Amount__c == null) {
			selectedWrappers[0].allocation.Amount__c = payment.Amount__c;
			totalAmount = payment.Amount__c;
		}

		//set allocation wrappers back to aura attributes with updated 'amount' and 'isSelected' properties
		for (var allocationName in allocationNameToWrappedAllocations) {
			if (allocationNameToWrappedAllocations[allocationName].length > 0)
				component.set("v." + allocationName, allocationNameToWrappedAllocations[allocationName]);
		}

		var numOfErrors = 0;

		if (selectedAllocationsWithNoAmount >= 1 && selectedWrappers.length > 1) {
			component.set("v.inSaving", false);
			this.showErrorMessage(component, 'Error', $A.get("$Label.c.ErrorMessage_AmountShouldBeProvided"));
			numOfErrors++;
		}

		if (totalAmount != payment.Amount__c) {
			component.set("v.inSaving", false);
			this.showErrorMessage(component, 'Error', $A.get("$Label.c.ErrorMessage_TotalAllocationsAmount"));
			numOfErrors++;
		}

		if (numOfErrors >= 1)
			check = false;

		return check;
	},

	updateAllocatePaymentSelectionState: function (component, allocationType, checked, sourceName) {
		var allocatePaymentSelectionState = component.get("v.allocatePaymentSelectionState");
		switch (allocationType) {
			case 'Invoice__c':
				if (checked) {
					allocatePaymentSelectionState.selectedInvoicesCount += 1;
					var invoiceId = sourceName;
					var wrappedInvoiceAllocations = component.get("v.wrappedInvoiceAllocations");
					for (var i in wrappedInvoiceAllocations) {
						var invoiceAllocation = wrappedInvoiceAllocations[i];
						if (invoiceAllocation.invoice.Id !== invoiceId && invoiceAllocation.isSelected == true) {
							allocatePaymentSelectionState.selectedInvoicesCount -= 1;
							invoiceAllocation.isSelected = false;
							component.set("v.wrappedInvoiceAllocations", wrappedInvoiceAllocations);
							break;
						}
					}
				}
				else {
					allocatePaymentSelectionState.selectedInvoicesCount -= 1;
				}
				break;
			case 'Agreement__c':
				allocatePaymentSelectionState.selectedAgreementsCount = checked ? allocatePaymentSelectionState.selectedAgreementsCount + 1
					: allocatePaymentSelectionState.selectedAgreementsCount - 1;
				break;
			case 'Fund__c':
				allocatePaymentSelectionState.selectedFundsCount = checked ? allocatePaymentSelectionState.selectedFundsCount + 1
					: allocatePaymentSelectionState.selectedFundsCount - 1;
				break;
			case 'Project__c':
				allocatePaymentSelectionState.selectedProjectsCount = checked ? allocatePaymentSelectionState.selectedProjectsCount + 1
					: allocatePaymentSelectionState.selectedProjectsCount - 1;
				break;
			case 'Campaign':
				allocatePaymentSelectionState.selectedCampaignsCount = checked ? allocatePaymentSelectionState.selectedCampaignsCount + 1
					: allocatePaymentSelectionState.selectedCampaignsCount - 1;
				break;
		}
		component.set("v.allocatePaymentSelectionState", allocatePaymentSelectionState);
	},

	validateAmounInput: function (component) {
		var amountInputs = component.find('amountInput');
		if (!amountInputs)
			return;
		var allValid = [].concat(amountInputs).reduce(function (validSoFar, inputComponent) {
			inputComponent.reportValidity();
			return validSoFar && inputComponent.checkValidity();
		}, true);
		return allValid;
	},

	isJsonString: function (string) {
		try {
			JSON.parse(string);
		} catch (exception) {
			return false;
		}
		return true;
	}
})