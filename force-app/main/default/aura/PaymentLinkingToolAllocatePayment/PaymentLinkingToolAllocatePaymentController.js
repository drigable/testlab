({
	initialize: function (component, event, helper) {
        var searchCriteria = {
            agreementName: '',
            agreementMediaCode: '',
            fundName: '',
            invoiceNumber: '',
            campaignName: '',
            campaignMediaCode: '',
            projectName: '',
            projectCode: ''
        }
        var contextData = component.get("v.contextData");
        if ((contextData.isParishUnion && !contextData.showAllocation) == false) {
            component.set("v.proceedToNormalAllocation", true);
        }
        if (contextData) {
            component.set("v.searchCriteria", searchCriteria);
            component.set("v.fieldLabels", contextData.fieldLabels);
            component.set("v.pickListValues", contextData.pickListValues);
            component.set("v.objectLabels", contextData.objectLabels);
            component.set("v.wrappedInvoiceAllocations", contextData.wrappedInvoiceAllocations);
            component.set("v.wrappedAgreementAllocations", contextData.wrappedAgreementAllocations);
            component.set("v.wrappedFundAllocations", contextData.wrappedFundAllocations);
            component.set("v.wrappedProjectAllocations", contextData.wrappedProjectAllocations);
            component.set("v.wrappedCampaignAllocations", contextData.wrappedCampaignAllocations);
            component.set("v.allocatePaymentSelectionState", contextData.allocatePaymentSelectionState);
        }
    },

    searchAllocations: function (component, event, helper) {
        helper.searchAllocations(component);
    },

    createAllocations: function (component, event, helper) {
        component.set("v.inSaving", true);

        var allValid = helper.validateAmounInput(component);
        if (!allValid) {
            helper.showErrorMessage(component, $A.get("$Label.c.ErrorMessage_SaveError"), $A.get("$Label.c.PaymentLinkingToolLightning_Error_AmountEnteredIsInvalid"));
            component.set("v.inSaving", false);
            return;
        }
        
        var contextData = component.get("v.contextData");

		var payment = contextData.selectedPayment;
		var wrappedInvoiceAllocations = component.get("v.wrappedInvoiceAllocations");
		var wrappedAgreementAllocations = component.get("v.wrappedAgreementAllocations");
		var wrappedFundAllocations = component.get("v.wrappedFundAllocations");
		var wrappedProjectAllocations = component.get("v.wrappedProjectAllocations");
		var wrappedCampaignAllocations = component.get("v.wrappedCampaignAllocations");
        
        var allocationNameToWrappedAllocations = {
            'wrappedInvoiceAllocations' : wrappedInvoiceAllocations[0] !== 'OUT_OF_SEARCH' ? wrappedInvoiceAllocations : [],
            'wrappedAgreementAllocations' : wrappedAgreementAllocations[0] !== 'OUT_OF_SEARCH' ? wrappedAgreementAllocations : [],
            'wrappedFundAllocations' : wrappedFundAllocations[0] !== 'OUT_OF_SEARCH' ? wrappedFundAllocations : [],
            'wrappedProjectAllocations' : wrappedProjectAllocations[0] !== 'OUT_OF_SEARCH' ? wrappedProjectAllocations : [],
            'wrappedCampaignAllocations' : wrappedCampaignAllocations[0] !== 'OUT_OF_SEARCH' ? wrappedCampaignAllocations : [],
        };
        
        var wrappedSelectedAllocationsMap = {};

		var selectedWrappers = [];
        var check = helper.checkTotalAmountAgainstPaymentAmount(component, payment, allocationNameToWrappedAllocations, wrappedSelectedAllocationsMap, selectedWrappers);

        if (check) {
            helper.createPaymentAllocations(
                component, payment, contextData.selectedAccount, selectedWrappers.length, wrappedSelectedAllocationsMap, contextData.createTransferPayments);
        }
            
    },

    previousStep: function (component, event, helper) {
        helper.switchCurrentComponent(component, "c:PaymentLinkingToolSelectAccount", true);
    },

    firstStep: function (component, event, helper) {
        helper.switchCurrentComponent(component, "c:PaymentLinkingToolSelectPayment", true);
    },

    handleSelectAll: function (component, event, helper) {
        var eventSource = event.getSource();
        var value = eventSource.get('v.value');
        var checked = eventSource.get('v.checked');
        var allocatePaymentSelectionState = component.get("v.allocatePaymentSelectionState");
        switch(value) {
            case 'agreements':
                var wrappedAgreementAllocations = component.get("v.wrappedAgreementAllocations");
                allocatePaymentSelectionState.selectedAgreementsCount = checked ? wrappedAgreementAllocations.length : 0;
                component.set("v.wrappedAgreementAllocations", changeAllSelection(wrappedAgreementAllocations, checked));
                break;
            case 'funds':
                var wrappedFundAllocations = component.get("v.wrappedFundAllocations");
                allocatePaymentSelectionState.selectedFundsCount = checked ? wrappedFundAllocations.length : 0;
                component.set("v.wrappedFundAllocations", changeAllSelection(wrappedFundAllocations, checked));
                break;
            case 'projects':
                var wrappedProjectAllocations = component.get("v.wrappedProjectAllocations");
                allocatePaymentSelectionState.selectedProjectsCount = checked ? wrappedProjectAllocations.length : 0;
                component.set("v.wrappedProjectAllocations", changeAllSelection(wrappedProjectAllocations, checked));
                break;
            case 'campaigns':
                var wrappedCampaignAllocations = component.get("v.wrappedCampaignAllocations");   
                allocatePaymentSelectionState.selectedCampaignsCount = checked ? wrappedCampaignAllocations.length : 0;
                component.set("v.wrappedCampaignAllocations", changeAllSelection(wrappedCampaignAllocations, checked));
                break;
        }
        function changeAllSelection(allocations, checked){
			for (var i in allocations) {
                allocations[i].isSelected = checked;
            }
            return allocations;
        };
        component.set("v.allocatePaymentSelectionState", allocatePaymentSelectionState);
    },

    handleSelectAllocation: function (component, event, helper) {
        var eventSource = event.getSource();
        var allocationType = eventSource.get('v.value');
        var checked = eventSource.get('v.checked');
        var sourceName = eventSource.get('v.name');
        helper.updateAllocatePaymentSelectionState(component, allocationType, checked, sourceName);
    },

    handleProceed: function (component, event, helper) {
        var contextData = component.get("v.contextData");
        var selectedPayment = contextData.selectedPayment;
        if (contextData.allocationType === '1') {
            component.set("v.proceedToNormalAllocation", true);
            return;
        }
        component.set("v.inProceed", true);
		var proceedAction = component.get("c.proceed");
		proceedAction.setParams({
			payment: selectedPayment,
			account: contextData.selectedAccount,
			ledgerAccount: contextData.ledgerAccount
		});
		proceedAction.setCallback(this, function (response) {
			if (response.getState() === "SUCCESS") {
                if (contextData.createTransferPayments) {
					helper.createTransferPayments(component, selectedPayment.Id, 'inProceed');
                }
                else {
                    component.set("v.inProceed", false);
                    helper.switchCurrentComponent(component, "c:PaymentLinkingToolSelectPayment", true, true);
                    helper.showSuccessMessage(component, null, $A.get("$Label.c.Message_SaveSuccessful"));
                }
			}
			else {
                helper.processErrorResponse(component, 'inProceed', response);
			}
		});
		$A.enqueueAction(proceedAction);
	},

    onBlurAmountInput: function (component, event, helper) {
        var eventSource = event.getSource();
        var amount = eventSource.get("v.value");
        if (amount && isNaN(amount)) {
            eventSource.setCustomValidity($A.get("$Label.c.PaymentLinkingToolLightning_Error_AmountEnteredIsInvalid"));
        } else {
            eventSource.setCustomValidity("");
        }
        eventSource.reportValidity();
    },

    enterPress: function(component, event, helper) {
        if (component.get("v.inSearching")) 
            return;
        var char = event.which || event.keyCode;
        if (component.get("v.proceedToNormalAllocation") && char == '13') {
            helper.searchAllocations(component);
        }
    }
})