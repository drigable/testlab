({
    afterRender: function (component, helper) {
        var afterRend = this.superAfterRender();
        helper.setFocusToComponentContainer(component);
        
        var nonInputSearchElements = component.find("card-wrapper");
        if (nonInputSearchElements) {
            [].concat(nonInputSearchElements).forEach(function (nonInputSearchElement) {
                nonInputSearchElement.getElement().addEventListener("keypress", helper.keypressFunction("stopPropagation"));
            });
        }
        return afterRend;
    }
})