({
	searchAccount: function (component, searchAction) {
		this.toggleSearchIcon(component);
		var searchTerm = component.get('v.cleanSearchTerm');
		var selectedIds = this.getSelectedIds(component);
		searchAction.setParams({
			searchTerm: searchTerm,
			selectedIds: selectedIds
		});

		searchAction.setCallback(this, function (response) {
			const state = response.getState();
			if (state === 'SUCCESS') {
				this.toggleSearchIcon(component);
				const returnValue = response.getReturnValue();
				component.set('v.searchResults', returnValue);
			}
			else if (state === 'ERROR') {
				this.toggleSearchIcon(component);
				const errors = response.getError();
				let message = 'Unknown error';
				if (errors && Array.isArray(errors) && errors.length > 0) {
					const error = errors[0];
					if (typeof error.message != 'undefined') {
						message = error.message;
					} else if (typeof error.pageErrors != 'undefined' && Array.isArray(error.pageErrors) && error.pageErrors.length > 0) {
						const pageError = error.pageErrors[0];
						if (typeof pageError.message != 'undefined') {
							message = pageError.message;
						}
					}
				}
				console.error('Error: ' + message);
				console.error(JSON.stringify(errors));
				const toastEvent = $A.get('e.force:showToast');
				if (typeof toastEvent !== 'undefined') {
					toastEvent.setParams({
						title: 'Server Error',
						message: message,
						type: 'error',
						mode: 'sticky'
					});
					toastEvent.fire();
				}
			}
		});

		$A.enqueueAction(searchAction);
	},
	updateSearchTerm: function (component, helper, searchTerm) {
		// Save search term so that it updates input
		component.set('v.searchTerm', searchTerm);

		// Get previous clean search term
		const cleanSearchTerm = component.get('v.cleanSearchTerm');
		// Compare clean new search term with current one and abort if identical
		const newCleanSearchTerm = searchTerm.trim().replace(/\*/g, '').toLowerCase();
		if (cleanSearchTerm === newCleanSearchTerm) {
			return;
		}

		// Update clean search term for later comparison
		component.set('v.cleanSearchTerm', newCleanSearchTerm);

		// Ignore search terms that are too small
		if (newCleanSearchTerm.length < 2) {
			component.set('v.searchResults', []);
			return;
		}

		// Apply search throttling (prevents search if user is still typing)
		let searchTimeout = component.get('v.searchThrottlingTimeout');
		if (searchTimeout) {
			clearTimeout(searchTimeout);
		}
		searchTimeout = window.setTimeout(
			$A.getCallback(function () {
				const searchTerm = component.get('v.searchTerm');
				if (searchTerm.length >= 2) {
					component.set("v.accountId", "");
					helper.toggleSearchIcon(component);
					var searchAction = component.get("c.search");
					helper.searchAccount(component, searchAction);
				}
				component.set('v.searchThrottlingTimeout', null);
			}),
			300
		);
		component.set('v.searchThrottlingTimeout', searchTimeout);
	},

	selectResult: function (component, recordId) {
		const searchResults = component.get('v.searchResults');
		const selectedResult = searchResults.filter(function (result) { return result.Id === recordId; });
		if (selectedResult.length > 0) {
			const selection = component.get('v.selection');
			selection.push(selectedResult[0]);
			component.set('v.selection', selection);
		}
		// Reset search
		component.set('v.searchTerm', '');
		component.set('v.searchResults', []);
		component.set('v.accountId', recordId);
	},

	getSelectedIds: function (component) {
		const selection = component.get('v.selection');
		return selection.map(function (element) { return element.id; });
	},

	removeSelectedItem: function (component, removedItemId) {
		const selection = component.get('v.selection');
		const updatedSelection = selection.filter(function (item) { return item.Id !== removedItemId; });
		component.set('v.selection', updatedSelection);
	},

	isSelectionAllowed: function (component) {
		return component.get('v.selection').length === 0;
	},

	toggleSearchIcon: function (component) {
		const searchIcon = component.find('search-icon');
		$A.util.toggleClass(searchIcon, 'slds-hide');
	}
})