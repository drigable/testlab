({
    initialize: function (component, event, helper) {
		var predefinedAccountId = component.get("v.accountId");
		var predefinedAccountName = component.get("v.accountName");
		var selection = component.get("v.selection");
		if (predefinedAccountId && predefinedAccountName) {
			var selection = component.get("v.selection");

			selection.push({Id: predefinedAccountId, Name: predefinedAccountName});
			component.set("v.selection", selection);
		} else if (predefinedAccountId && !predefinedAccountName) {
			selection.push({Id: predefinedAccountId, Name: predefinedAccountId});
		}
		component.set("v.selection", selection);
	},
	
	onInput : function(component, event, helper) {
		// Prevent action if selection is not allowed
		if (!helper.isSelectionAllowed(component)) {
			return;
		}
		const newSearchTerm = event.target.value;
		helper.updateSearchTerm(component, helper, newSearchTerm);
	},

	onResultClick : function(component, event, helper) {
		const recordId = event.currentTarget.id;
		helper.selectResult(component, recordId);
	},

	onComboboxClick : function(component, event, helper) {
		// Hide combobox immediatly
		const blurTimeout = component.get('v.blurTimeout');
		if (blurTimeout) {
			clearTimeout(blurTimeout);
		}
		component.set('v.hasFocus', false);
	},

	onFocus : function(component, event, helper) {
		// Prevent action if selection is not allowed
		if (!helper.isSelectionAllowed(component)) {
			return;
		}
		component.set('v.hasFocus', true);
	},

	onBlur : function(component, event, helper) {
		// Prevent action if selection is not allowed
		if (!helper.isSelectionAllowed(component)) {
			return;
		}
		// Delay hiding combobox so that we can capture selected result
		const blurTimeout = window.setTimeout(
			$A.getCallback(function() {
				component.set('v.hasFocus', false);
				component.set('v.blurTimeout', null);
			}),
			300
		);
		component.set('v.blurTimeout', blurTimeout);
	},

	onRemoveSelectedItem : function(component, event, helper) {
		const itemId = event.getSource().get('v.name');
		helper.removeSelectedItem(component, itemId);
	},

	onClearSelection : function(component, event, helper) {
		component.set("v.accountId", "");
		component.set('v.selection', []);
	}
})