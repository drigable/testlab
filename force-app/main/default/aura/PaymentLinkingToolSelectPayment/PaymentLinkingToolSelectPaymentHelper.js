({
    searchPayments: function (component) {
        var inSearching = component.get("v.inSearching");
        if (inSearching) {
            return;
        }
        else {
            component.set("v.inSearching", true);
        }
        var contextData = component.get("v.contextData");
        contextData.selectedPayment = null;
        var getWrappedPaymentsAction = component.get("c.getWrappedPayments");
        var searchCriteriaData = component.get("v.searchCriteria");
        component.set("v.contextData", contextData);
        getWrappedPaymentsAction.setParams({searchCriteria: searchCriteriaData});
        getWrappedPaymentsAction.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                var responseData = response.getReturnValue();
                component.set("v.inSearching", false);
                component.set("v.paymentWrappers", responseData.records);
                component.set("v.hasMorePayments", responseData.hasMore);
            }
            else {
                component.set("v.inSearching", false);
                component.set("v.paymentWrappers", []);
                component.set("v.hasMorePayments", false);
                var errors = response.getError();
                var errorMessage;
                if (errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                }
                this.showErrorMessage(component, $A.get("$Label.c.PaymentLinkingToolLightning_Error_ErrorOccurredOnItemRetrieval"), errorMessage);
            }
        });
        window.setTimeout($A.getCallback(function() {
            $A.enqueueAction(getWrappedPaymentsAction);
        }), 100);
    },

    switchSelectPaymentComponent: function (component) {
        var contextData = component.get("v.contextData");
        contextData.searchCriteria.selectPayment = component.get("v.searchCriteria");
        contextData.paymentWrappers = component.get("v.paymentWrappers");
        contextData.hasMorePayments = component.get("v.hasMorePayments");
        var selectedPayment = contextData.selectedPayment;
        if ( !contextData.accountByPayerNameFound && (selectedPayment.PayerName__c != null && selectedPayment.PayerName__c !== '') ) {   
            contextData.searchCriteria.selectAccount = {
                accountName: selectedPayment.PayerName__c,
                addressStreet: '',
                addressCity: '',
                addressPostalCode: ''
            };
            var getWrappedAccountsAction = component.get("c.getWrappedAccounts");
            getWrappedAccountsAction.setParams({searchCriteria: contextData.searchCriteria.selectAccount});
            getWrappedAccountsAction.setCallback(this, function (response) {
                if (response.getState() === "SUCCESS") {
                    var accountWrappers = response.getReturnValue().records;
                    if (accountWrappers.length === 1) {
                        contextData.accountByPayerNameFound = true;
                        var account = accountWrappers[0].account;
                        accountWrappers[0].isSelected = true;
                        if (account.Type__c === contextData.parishUnionString && account.MemberParishes__r && account.MemberParishes__r !== []) {
                            contextData.isParishUnion = true;
                            contextData.memberParishes = account.MemberParishes__r;
                            contextData.showAllocation = false;
                        } else {
                            contextData.isParishUnion = false;
                            contextData.showAllocation = true;
                        }
                        contextData.selectedAccount = account;
                        contextData.accountWrappers = accountWrappers;
                        component.set("v.contextData", contextData);
                        this.switchCurrentComponent(component, "c:PaymentLinkingToolAllocatePayment");
                    } else {
                        contextData.searchCriteria.selectAccount = {};
                        component.set("v.contextData", contextData);
                        this.switchCurrentComponent(component, "c:PaymentLinkingToolSelectAccount");
                    }
                    
                }
                else {
                    var errors = response.getError();
                    var errorMessage;
                    if (errors[0] && errors[0].message) {
                        errorMessage = errors[0].message;
                    }
                    this.showErrorMessage(component, $A.get("$Label.c.PaymentLinkingToolLightning_Error_ErrorOccurredOnItemRetrieval"), errorMessage);
                }
            });
            $A.enqueueAction(getWrappedAccountsAction);
        } else {
            component.set("v.contextData", contextData);
            this.switchCurrentComponent(component, "c:PaymentLinkingToolSelectAccount");
        }
    }
})