({
	initialize: function (component, event, helper) {
        component.set("v.searchCriteria", {
            selectedReferenceType: '',
            sLSbankAccountNumber: '',
            transactionDateFrom: null,
            transactionDateTo: null
        });
        var contextData = component.get("v.contextData")
        if (contextData) {
            component.set("v.fieldLabels", contextData.fieldLabels);
            component.set("v.pickListValues", contextData.pickListValues);
            component.set("v.objectLabels", contextData.objectLabels);
            if (contextData.searchCriteria.selectPayment != {}) {
                component.set("v.searchCriteria", contextData.searchCriteria.selectPayment);
            }
            if (contextData.paymentWrappers.length > 0) {
                component.set("v.paymentWrappers", contextData.paymentWrappers);
                component.set("v.hasMorePayments", contextData.hasMorePayments);
            } else {
                helper.searchPayments(component);
            }
        }
    },

    searchPayments: function (component, event, helper) {
        helper.searchPayments(component);
    },

    nextStep: function (component, event, helper) {
        helper.switchSelectPaymentComponent(component);
    },

    doCancel: function (component, event, helper) {
        helper.switchCurrentComponent(component, "c:PaymentLinkingToolSelectPayment", true, false);
    },

    handleSelectPayment: function (component, event, helper) {
        var contextData = component.get("v.contextData");
        contextData.accountByPayerNameFound = false;
        contextData.selectedAccount = null;
        contextData.accountWrappers = ['OUT_OF_SEARCH'];
        contextData.hasMoreAccounts = false;
        contextData.isParishUnion = false;
        contextData.memberParishes = [];
        var paymentWrappers = component.get("v.paymentWrappers");
        var paymentId = event.getSource().get('v.value');
        for (var i in paymentWrappers) {
            if (paymentWrappers[i].payment.Id === paymentId) {
                contextData.selectedPayment = paymentWrappers[i].payment;
            } else if (paymentWrappers[i].payment.Id !== paymentId && paymentWrappers[i].isSelected) {
                paymentWrappers[i].isSelected = false;
            }
        }
        component.set("v.paymentWrappers", paymentWrappers);
        component.set("v.contextData", contextData);
    },

    enterPress: function(component, event, helper) {
        if (component.get("v.inSearching")) 
            return;
        var char = event.which || event.keyCode;
        if (char == '13') {
            helper.setFocusToComponentContainer(component);
            helper.searchPayments(component);
        }
    }
})