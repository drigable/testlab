({
    afterRender: function (component, helper) {
        var afterRend = this.superAfterRender();
        helper.setFocusToComponentContainer(component);

        var nonInputSearchElements = component.find("search-result-card-container");
        if (nonInputSearchElements) {
            [].concat(nonInputSearchElements).forEach(function (nonInputSearchElement) {
                nonInputSearchElement.getElement().addEventListener("keypress", helper.keypressFunction("stopPropagation"));
            });
        }
        var inputSearchElements = component.find("input-search-element");
        if (inputSearchElements) {
            [].concat(inputSearchElements).forEach(function (inputSearchElement) {
                inputSearchElement.getElement().addEventListener("keypress", helper.keypressFunction("preventDefault"), true);
            });
        }
        return afterRend;
    }
})
