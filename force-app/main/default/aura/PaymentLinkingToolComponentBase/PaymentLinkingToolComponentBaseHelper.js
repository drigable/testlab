({
	switchCurrentComponent: function (component, nextComponentName, resetContextData, fullResetWithSavingFilterState) {
        if (resetContextData) {
            this.resetContextData(component, nextComponentName, fullResetWithSavingFilterState);
        }
        var navigationEvent = component.getEvent("navigate");
        navigationEvent.setParam("nextComponentName", nextComponentName);
        navigationEvent.setParam("contextData", component.get("v.contextData"));
        navigationEvent.fire();
    },

    resetContextData: function (component, nextComponentName, fullResetWithSavingFilterState) {
        var contextData = component.get("v.contextData");
        
        switch(component.getName()) {
            case 'cPaymentLinkingToolSelectPayment':
                this.resetSelectPayment(contextData, fullResetWithSavingFilterState);
                this.resetSelectAccount(contextData);
                break;
            case 'cPaymentLinkingToolSelectAccount':
                this.resetSelectAccount(contextData);
                this.resetAllocatePayment(contextData);
                if (fullResetWithSavingFilterState != undefined)
                    this.resetSelectPayment(contextData, fullResetWithSavingFilterState);
                break;
            case 'cPaymentLinkingToolAllocatePayment':
                this.resetAllocatePayment(contextData);
                if (nextComponentName === 'c:PaymentLinkingToolSelectPayment') {
                    this.resetSelectAccount(contextData);
                    if (fullResetWithSavingFilterState != undefined)
                        this.resetSelectPayment(contextData, fullResetWithSavingFilterState);
                }
                break;
        }
        
        component.set("v.contextData", contextData);
    },

    resetSelectPayment: function (contextData, fullResetWithSavingFilterState) {
        if (fullResetWithSavingFilterState === false) {
            contextData.searchCriteria.selectPayment = {};
        }
        contextData.paymentWrappers = [];
        contextData.hasMorePayments = false;
        contextData.selectedPayment = null;
    },

    resetSelectAccount: function (contextData) {
        contextData.searchCriteria.selectAccount = {};
        contextData.accountByPayerNameFound = false;
        contextData.isParishUnion = false;
        contextData.memberParishes = [];
        contextData.showAllocation = null;
        contextData.accountWrappers = ['OUT_OF_SEARCH'];
        contextData.hasMoreAccounts = false;
        contextData.selectedAccount = null;
    },

    resetAllocatePayment: function (contextData) {
        contextData.searchCriteria.allocatePayment = {};
        contextData.wrappedInvoiceAllocations = ['OUT_OF_SEARCH'];
        contextData.wrappedAgreementAllocations = ['OUT_OF_SEARCH'];
        contextData.wrappedFundAllocations = ['OUT_OF_SEARCH'];
        contextData.wrappedProjectAllocations = ['OUT_OF_SEARCH'];
        contextData.wrappedCampaignAllocations = ['OUT_OF_SEARCH'];
        contextData.allocatePaymentSelectionState = {
            selectedInvoicesCount: 0,
            selectedAgreementsCount: 0,
            selectedFundsCount: 0,
            selectedProjectsCount: 0,
            selectedCampaignsCount: 0
        }
        contextData.allocationType = '';
        contextData.ledgerAccount = null;
        contextData.createTransferPayments = false;
    },

    showMessage: function (component, title, message, variant) {
        var notificationsLibrary = component.getSuper().find("notificationsLibrary");
        notificationsLibrary.showToast({
            title: title,
            message: message,
            variant: variant
        });
    },

    showSuccessMessage: function (component, title,  message) {
        this.showMessage(component, title,  message, "SUCCESS");
    },

    showErrorMessage: function (component, title,  message) {
        this.showMessage(component, title,  message, "ERROR");
    },

    keypressFunction: function(eventProperty) {
        return function(e) {
            var char = e.which || e.keyCode;
            if (char == 13 && e[eventProperty]) {
                e[eventProperty]();
            } 
        };
    },

    setFocusToComponentContainer: function(component) {
        var componentContainer = component.find("component-container");
        if (!componentContainer)
            return;

        var componentContainerElement = componentContainer.getElement();
        if (componentContainerElement) {
            componentContainerElement.focus();
        }
    }
})