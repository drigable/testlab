({
    searchAccounts: function (component) {
        component.set("v.inSearching", true);
		var contextData = component.get("v.contextData");
		contextData.selectedAccount = null;
		component.set("v.contextData", contextData);

		var getWrappedAccountsAction = component.get("c.getWrappedAccounts");
	    getWrappedAccountsAction.setParams({searchCriteria: component.get("v.searchCriteria")});
		getWrappedAccountsAction.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                var responseData = response.getReturnValue();
                var accountWrappers = responseData.records;
                component.set("v.inSearching", false);
                if (accountWrappers.length === 1) {
                    accountWrappers[0].isSelected = true;
                    var contextData = component.get("v.contextData");
                    var account = accountWrappers[0].account;
                    contextData.selectedAccount = account;
                    if (account.Type__c === contextData.parishUnionString && account.MemberParishes__r && account.MemberParishes__r !== []) {
                        contextData.isParishUnion = true;
                        contextData.memberParishes = account.MemberParishes__r;
                        contextData.showAllocation = false;
                    } else {
                        contextData.isParishUnion = false;
                        contextData.showAllocation = true;
                    }
                    contextData.accountWrappers = accountWrappers;
                    component.set("v.contextData", contextData);
                }
                component.set("v.accountWrappers", accountWrappers);
                component.set("v.hasMoreAccounts", responseData.hasMore);
            }
            else {
                component.set("v.inSearching", false);
                component.set("v.accountWrappers", ['OUT_OF_SEARCH']);
                component.set("v.hasMoreAccounts", false);
                var errors = response.getError();
                var errorMessage;
                if (errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                }
                this.showErrorMessage(component, $A.get("$Label.c.PaymentLinkingToolLightning_Error_ErrorOccurredOnItemRetrieval"), errorMessage);
            }
        });
        window.setTimeout($A.getCallback(function() {
            $A.enqueueAction(getWrappedAccountsAction);
        }), 100);
    }
})