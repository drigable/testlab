({
	initialize: function (component, event, helper) {
        
        component.set("v.searchCriteria", {
            accountName: '',
            addressStreet: '',
            addressCity: '',
            addressPostalCode: ''
        });
        var contextData = component.get("v.contextData");
        if (contextData) {
            component.set("v.fieldLabels", contextData.fieldLabels);
            component.set("v.pickListValues", contextData.pickListValues);
            component.set("v.objectLabels", contextData.objectLabels);
            if (contextData.searchCriteria.selectAccount != {}) {
                component.set("v.searchCriteria", contextData.searchCriteria.selectAccount);
            }
            component.set("v.accountWrappers", contextData.accountWrappers);
            component.set("v.hasMoreAccounts", contextData.hasMoreAccounts);
        }
    },

    searchAccounts: function (component, event, helper) {
        helper.searchAccounts(component);
    },

    previousStep: function (component, event, helper) {
        var contextData = component.get("v.contextData");

        contextData.searchCriteria.selectAccount = component.get("v.searchCriteria");
        contextData.accountWrappers = component.get("v.accountWrappers");
        contextData.hasMoreAccounts = component.get("v.hasMoreAccounts");

        component.set("v.contextData", contextData);
        helper.switchCurrentComponent(component, "c:PaymentLinkingToolSelectPayment", true);
    },

    nextStep: function (component, event, helper) {
        var contextData = component.get("v.contextData");

        contextData.searchCriteria.selectAccount = component.get("v.searchCriteria");
        contextData.accountWrappers = component.get("v.accountWrappers");
        contextData.hasMoreAccounts = component.get("v.hasMoreAccounts");

        var selectedAccount = contextData.selectedAccount;
        var selectedPayment = contextData.selectedPayment;
        var isParishUnion = contextData.isParishUnion;

        if (selectedPayment.Message__c && selectedPayment.Message__c !== '') {   
            
            var searchCampaignMediaCodeInPaymentMsg = component.get("c.searchCampaignMediaCodeInPaymentMsg");
            var searchProjectCodeInPaymentMsg = component.get("c.searchProjectCodeInPaymentMsg");

            searchCampaignMediaCodeInPaymentMsg.setParams({
                payment: selectedPayment,
                account: selectedAccount,
                isParishUnion: isParishUnion
            });
            searchProjectCodeInPaymentMsg.setParams({
                payment: selectedPayment,
                account: selectedAccount
            });

            searchCampaignMediaCodeInPaymentMsg.setCallback(this, function (response) {
                if (response.getState() === "SUCCESS") {
                    var wrappedCampaignAllocations = response.getReturnValue();
                    if (wrappedCampaignAllocations && wrappedCampaignAllocations.length > 0) {
                        contextData.wrappedCampaignAllocations = wrappedCampaignAllocations;
                    }
                } else {
                    var errors = response.getError();
                    var errorMessage;
                    if (errors[0] && errors[0].message) {
                        errorMessage = errors[0].message;
                    }
                    helper.showErrorMessage(component, $A.get("$Label.c.ErrorMessage_SearchCampaigns"), errorMessage);
                }
                $A.enqueueAction(searchProjectCodeInPaymentMsg);
            });
            searchProjectCodeInPaymentMsg.setCallback(this, function (response) {
                if (response.getState() === "SUCCESS") {
                    var wrappedProjectAllocations = response.getReturnValue();
                    if (wrappedProjectAllocations && wrappedProjectAllocations.length > 0) {
                        contextData.wrappedProjectAllocations = wrappedProjectAllocations;
                    } 
                } else {
                    var errors = response.getError();
                    var errorMessage;
                    if (errors[0] && errors[0].message) {
                        errorMessage = errors[0].message;
                    }
                    helper.showErrorMessage(component, $A.get("$Label.c.ErrorMessage_SearchProjects"), errorMessage);
                }
                component.set("v.contextData", contextData);
                helper.switchCurrentComponent(component, "c:PaymentLinkingToolAllocatePayment");
            });
            $A.enqueueAction(searchCampaignMediaCodeInPaymentMsg);
        } else {
            component.set("v.contextData", contextData);
            helper.switchCurrentComponent(component, "c:PaymentLinkingToolAllocatePayment");
        }
    },

    doCancel: function (component, event, helper) {
        helper.switchCurrentComponent(component, "c:PaymentLinkingToolSelectPayment", true, false);
    },

    createAccount: function (component, event, helper) {
        var contextData = component.get("v.contextData");

        contextData.searchCriteria.selectAccount = component.get("v.searchCriteria");
        contextData.accountWrappers = component.get("v.accountWrappers");
        contextData.hasMoreAccounts = component.get("v.hasMoreAccounts");

        component.set("v.contextData", contextData);
        helper.switchCurrentComponent(component, "c:PaymentLinkingToolCreateAccount");
    },

    handleSelectAccount: function (component, event, helper) {
        var contextData = component.get("v.contextData");
        var accountWrappers = component.get("v.accountWrappers");
        var selectedAccountId = event.getSource().get('v.value');
        for (var i in accountWrappers) {
            if (accountWrappers[i].account.Id === selectedAccountId) {
                var account = accountWrappers[i].account;
                contextData.selectedAccount = account
                contextData.isParishUnion = account.Type__c === contextData.parishUnionString;
                contextData.showAllocation = !contextData.isParishUnion;
                contextData.memberParishes = contextData.isParishUnion ? account.MemberParishes__r : [];
            } else if (accountWrappers[i].account.Id !== selectedAccountId && accountWrappers[i].isSelected) {
                accountWrappers[i].isSelected = false;
            }
        }
        component.set("v.accountWrappers", accountWrappers);
        component.set("v.contextData", contextData);
    },

    enterPress: function(component, event, helper) {
        if (component.get("v.inSearching")) 
            return;
        var char = event.which || event.keyCode;
        if (char == '13') {
            var searchButton = component.find("search-button");
            if (searchButton && searchButton.get("v.disabled")) 
                return;
            helper.searchAccounts(component);
        }
    }
})