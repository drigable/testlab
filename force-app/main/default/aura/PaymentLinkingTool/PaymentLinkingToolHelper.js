({
	insertComponent: function (component, childComponentName, contextData) {
        $A.createComponent(childComponentName,
            {
               "contextData": contextData
            },
            function(childComponent, status, errorMessage) {
                component.set("v.body", [childComponent]);
            }
        );
    },
    
    showErrorMessage: function (component, title, message) {
        var notificationsLibrary = component.find("notificationsLibrary");
        notificationsLibrary.showToast({
            title: title,
            message: message,
            variant: "ERROR"
        });
    }
})