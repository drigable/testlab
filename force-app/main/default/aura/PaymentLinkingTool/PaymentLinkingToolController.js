({
	initialize: function (component, event, helper) {
        var OUT_OF_SEARCH = 'OUT_OF_SEARCH';
        var contextData = {
            searchCriteria: {selectPayment: {}, selectAccount: {}, allocatePayment: {}},
            fieldLabels: {},
            pickListValues: {},
            objectLabels: {},
            paymentWrappers: [],
            selectedPayment: null,
            accountWrappers: [OUT_OF_SEARCH],
            selectedAccount: null,
            accountRecordTypeOptions: [],
            paymentAllocationOptions: [
                { value: "1", label: '1. ' + $A.get("$Label.c.Message_ProceedToNormalAllocation") },
                { value: "2", label: '2. ' + $A.get("$Label.c.Message_LinkToMemberParishAgreements") }
            ],
            allocationType: '',
            allocatePaymentSelectionState: {
                selectedInvoicesCount: 0,
                selectedAgreementsCount: 0,
                selectedFundsCount: 0,
                selectedProjectsCount: 0,
                selectedCampaignsCount: 0
            },
            wrappedInvoiceAllocations: [OUT_OF_SEARCH],
            wrappedAgreementAllocations: [OUT_OF_SEARCH],
            wrappedFundAllocations: [OUT_OF_SEARCH],
            wrappedProjectAllocations: [OUT_OF_SEARCH],
            wrappedCampaignAllocations: [OUT_OF_SEARCH],
            isParishUnion: false,
            showAllocation: false,
            createTransferPayments: false,
            parishUnionString: 'Seurakuntayhtymä',
            memberParishes: null,
            ledgerAccount: null,
            accountByPayerNameFound: false
        };
        
        var action1 = component.get("c.getSobjectApiNameToFieldLabelMap");
        var action2 = component.get("c.getSobjectApiNameToLabelMap");
        var action3 = component.get("c.getFieldApiNameToPickListValuesMap");
        var action4 = component.get("c.getAccountRecordTypeOptions");
        action1.setParams({ sobjectApiNames : ['Payment__c', 'Voucher__c', 'Account', 'Campaign', 'Agreement__c', 'Project__c', 'Fund__c', 'Invoice__c'] });
        action2.setParams({ sobjectApiNames : ['Agreement__c', 'Campaign', 'Account', 'Payment__c', 'Order__c', 'Product__c', 'Fund__c', 'Invoice__c', 'Project__c'] });
        action3.setParams({ 
            sobjectApiNameToFieldApiNamesMap : {'Payment__c': ['SLSbankAccountNumber__c', 'LedgerAccount__c']} 
        });
        
        action1.setCallback(this, $A.getCallback(function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                contextData.fieldLabels = response.getReturnValue();
                $A.enqueueAction(action2);
            } else {
                var errors = response.getError();
                var errorMessage;
                if (errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                }
                helper.showErrorMessage(component, $A.get("$Label.c.PaymentLinkingToolLightning_Error_AnErrorOccurredOnDataInitializing"), errorMessage);
            }
        }));
        action2.setCallback(this, $A.getCallback(function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                contextData.objectLabels = response.getReturnValue();
                $A.enqueueAction(action3);
            } else {
                var errors = response.getError();
                var errorMessage;
                if (errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                }
                helper.showErrorMessage(component, $A.get("$Label.c.PaymentLinkingToolLightning_Error_AnErrorOccurredOnDataInitializing"), errorMessage);
            }
        }));               
        action3.setCallback(this, $A.getCallback(function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var pickListValues = {};
                pickListValues = response.getReturnValue();
                pickListValues.SLSbankAccountNumber__c.unshift({value: "", label: $A.get("$Label.c.None_PicklistValue")});
                pickListValues.LedgerAccount__c.unshift({value: "", label: $A.get("$Label.c.None_PicklistValue")});
                
                pickListValues.referenceType = [
                    { value: "", label: $A.get("$Label.c.None_PicklistValue") },
                    { value: "1", label: '1 - ' + contextData.objectLabels.Agreement__c.label },
                    { value: "3", label: '3 - ' + $A.get("$Label.c.MagazineOrder") },
                    { value: "4", label: '4 - ' + contextData.objectLabels.Campaign.label }
                ];
                
                contextData.pickListValues = pickListValues;
                $A.enqueueAction(action4);
            } else {
                var errors = response.getError();
                var errorMessage;
                if (errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                }
                helper.showErrorMessage(component, $A.get("$Label.c.PaymentLinkingToolLightning_Error_AnErrorOccurredOnDataInitializing"), errorMessage);
            }
        }));
        action4.setCallback(this, $A.getCallback(function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var accountRecordTypeOptions = response.getReturnValue();
                if (accountRecordTypeOptions) {
                    contextData.accountRecordTypeOptions = accountRecordTypeOptions;
                }
                helper.insertComponent(component, "c:PaymentLinkingToolSelectPayment", contextData);
            } else {
                var errors = response.getError();
                var errorMessage;
                if (errors[0] && errors[0].message) {
                    errorMessage = errors[0].message;
                }
                helper.showErrorMessage(component, $A.get("$Label.c.PaymentLinkingToolLightning_Error_AnErrorOccurredOnDataInitializing"), errorMessage);
            }
        }));
        $A.enqueueAction(action1);		
	},
    
    switchCurrentComponent: function (component, event, helper) {
        var navigationParams = event.getParams();
        helper.insertComponent(component, navigationParams.nextComponentName, navigationParams.contextData);
    }
})