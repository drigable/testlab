({
	update : function(component, event, helper) {
		var updatePercentAction = component.get("c.updatePercent");
		var alignmentPlanId = component.get("v.recordId");
        updatePercentAction.setParams({
            recordId: alignmentPlanId,
            triggerAlignmentPlan: null
        });
        updatePercentAction.setCallback(this, function (response) {
			if (response.getState() === "SUCCESS") {
				console.log('SUCSSES!');	
			}
			else {
				var errors = response.getError();
				var errorMessage;
				if (errors[0] && errors[0].message) {
					errorMessage = errors[0].message;
                    console.log('ERROR MESSAGE ', errorMessage);
				}
				
			}
		});
		$A.enqueueAction(updatePercentAction);
	}
})