trigger AlignmentPlanTrigger on Alignment_Plan__c (before update) {

    new AlignmentPlanTriggerHandler().run();

}