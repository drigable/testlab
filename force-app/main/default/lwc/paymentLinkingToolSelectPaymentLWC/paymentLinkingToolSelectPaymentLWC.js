import { LightningElement, api, track } from 'lwc';
import cancel from '@salesforce/label/c.Cancel';
import next from '@salesforce/label/c.Next';

export default class PaymentLinkingToolSelectPaymentLWC extends LightningElement {

    @api contextData = {};
    @track searchCriteria = {
        selectedReferenceType: '',
        sLSbankAccountNumber: '',
        transactionDateFrom: null,
        transactionDateTo: null
    }

    label = {
        cancel,
        next
    };
    pages = {firstPage: 'firstPage', secondPage: 'secondPage', thirdPage: 'thirdPage'};

    connectedCallback() {
        // var contextData = component.get("v.contextData")
        // if (contextData) {
        //     component.set("v.fieldLabels", contextData.fieldLabels);
        //     component.set("v.pickListValues", contextData.pickListValues);
        //     component.set("v.objectLabels", contextData.objectLabels);
        //     if (contextData.searchCriteria.selectPayment != {}) {
        //         component.set("v.searchCriteria", contextData.searchCriteria.selectPayment);
        //     }
        //     if (contextData.paymentWrappers.length > 0) {
        //         component.set("v.paymentWrappers", contextData.paymentWrappers);
        //         component.set("v.hasMorePayments", contextData.hasMorePayments);
        //     } else {
        //         helper.searchPayments(component);
        //     }
        // }
    }

    nextStep() {

    }

    doCancel() {

    }

    nextHandler() {
        const switchCurrentComponentEvent = new CustomEvent(
            'switchcurrentcomponent', 
            { detail: {nextComponent: this.pages.secondPage, contextData: this.contextData} }
        );
        this.dispatchEvent(switchCurrentComponentEvent);
    }
}