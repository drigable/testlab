import { LightningElement, api, track } from 'lwc';

export default class PaymentLinkingToolSelectAccountLWC extends LightningElement {

    @api contextData = {};
    @track searchCriteria = {
        selectedReferenceType: '',
        sLSbankAccountNumber: '',
        transactionDateFrom: null,
        transactionDateTo: null
    }

    // label = {
    //     cancel,
    //     next
    // };
    pages = {firstPage: 'firstPage', secondPage: 'secondPage', thirdPage: 'thirdPage'};


    previousHandler() {
        const switchCurrentComponentEvent = new CustomEvent(
            'switchcurrentcomponent', 
            { detail: {nextComponent: this.pages.firstPage, contextData: this.contextData} }
        );
        this.dispatchEvent(switchCurrentComponentEvent);
    }

}