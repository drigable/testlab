import {LightningElement as LE, track} from 'lwc';

import proceedToNormalAllocation from '@salesforce/label/c.Message_ProceedToNormalAllocation';
import linkToMemberParishAgreements from '@salesforce/label/c.Message_LinkToMemberParishAgreements';

export default class PaymentLinkingToolLWC extends LE {
    OUT_OF_SEARCH = 'OUT_OF_SEARCH';
    displayComponent = true;
    @track contextData = {
        searchCriteria: {selectPayment: {}, selectAccount: {}, allocatePayment: {}},
        fieldLabels: {},
        pickListValues: {},
        objectLabels: {},
        paymentWrappers: [],
        selectedPayment: null,
        accountWrappers: [this.OUT_OF_SEARCH],
        selectedAccount: null,
        accountRecordTypeOptions: [],
        paymentAllocationOptions: [
            { value: "1", label: '1. ' + proceedToNormalAllocation },
            { value: "2", label: '2. ' + linkToMemberParishAgreements }
        ],
        allocationType: '',
        allocatePaymentSelectionState: {
            selectedInvoicesCount: 0,
            selectedAgreementsCount: 0,
            selectedFundsCount: 0,
            selectedProjectsCount: 0,
            selectedCampaignsCount: 0
        },
        wrappedInvoiceAllocations: [this.OUT_OF_SEARCH],
        wrappedAgreementAllocations: [this.OUT_OF_SEARCH],
        wrappedFundAllocations: [this.OUT_OF_SEARCH],
        wrappedProjectAllocations: [this.OUT_OF_SEARCH],
        wrappedCampaignAllocations: [this.OUT_OF_SEARCH],
        isParishUnion: false,
        showAllocation: false,
        createTransferPayments: false,
        parishUnionString: 'Seurakuntayhtymä',
        memberParishes: null,
        ledgerAccount: null,
        accountByPayerNameFound: false
    };

    @track display = {firstPage: true, secondPage: false, thirdPage: false};

    connectedCallback() {
    }

    renderedCallback() {
        // for (let page in this.display) {
        //     if (!this.display.hasOwnProperty(page))
        //         continue;
        //     if()
        // }
        const childConponentLWC1 = this.template.querySelector('c-payment-linking-tool-select-payment-l-w-c');
        const childConponentLWC2 = this.template.querySelector('c-payment-linking-tool-select-account-l-w-c');
        const childConponentLWC3 = this.template.querySelector('c-payment-linking-tool-allocate-payment-l-w-c');
        if (childConponentLWC1)
            childConponentLWC1.contextData = this.contextData;
        else if (childConponentLWC2)
            childConponentLWC2.contextData = this.contextData;
        else if (childConponentLWC3)
            childConponentLWC3.contextData = this.contextData;
    }

    //comment
    switchCurrentComponentHandler (event) {
        for (let page in this.display) {
            if (!this.display.hasOwnProperty(page))
                continue;
            if (this.display[page]) {
                this.display[page] = false;
                this.display[event.detail.nextComponent] = true;
                this.contextData = event.detail.contextData;
                break;  
            }
        }
    }

}