public virtual class TriggerHandlerBase {

    public void run() {
        if (Trigger.isExecuting) {
            switch on Trigger.operationType {
                when BEFORE_INSERT {
                    this.onBeforeInsert();
                }
                when BEFORE_UPDATE {
                    this.onBeforeUpdate();
                }
                when BEFORE_DELETE {
                    this.onBeforeDelete();
                }
                when AFTER_INSERT {
                    this.onAfterInsert();
                }
                when AFTER_UPDATE {
                    this.onAfterUpdate();
                }
                when AFTER_DELETE {
                    this.onAfterDelete();
                }
                when AFTER_UNDELETE {
                    this.onAfterUndelete();
                }
            }
        }
    }

    protected virtual void onBeforeInsert(){}

    protected virtual void onBeforeUpdate(){}

    protected virtual void onBeforeDelete(){}

    protected virtual void onAfterInsert(){}

    protected virtual void onAfterUpdate(){}

    protected virtual void onAfterDelete(){}

    protected virtual void onAfterUndelete(){}

}