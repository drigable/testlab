public without sharing class AlignmentPlanTriggerHandler extends TriggerHandlerBase{

    protected override void onBeforeUpdate() {
        AlignmentPlanHelper.switchRecordTypeControlledByStatus((List<Alignment_Plan__c>)Trigger.new);
        AlignmentPlanHelper.distributeAlignmentPlansOnUpdate((Map<Id, Alignment_Plan__c>)Trigger.newMap, (Map<Id, Alignment_Plan__c>)Trigger.oldMap);
	}

}