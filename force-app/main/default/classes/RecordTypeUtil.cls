public without sharing class RecordTypeUtil {
    static final Set<String> objects = new Set<String> { 'Account', 'Contact', 'Agreement__c', 'Project__c', 'Campaign', 'Payment__c', 'Invoice__c', 'Order__c' };
    
    public static Id personAccountRtId { get; private set; }
    public static Id organisationRtId { get; private set; }
    public static Id foreignPartnerRtId { get; private set; }
    public static Id contactRtId { get; private set; }
    public static Id employeeRtId { get; private set; }
    public static Id monthlyAgreementRtId { get; private set; }
    public static Id rentalAgreementRtId { get; private set; }
    public static Id serviceAgreementRtId { get; private set; }
    public static Id tributeeAgreementRtId { get; private set; }
    public static Id communityAgreementRtId { get; private set; }
    public static Id spontaneousAgreementRtId { get; private set; }
    public static Id projectRtId { get; private set; }
    public static Id projectRegionRtId { get; private set; }
    public static Id projectFelmRtId { get; private set; }
    public static Id projectActivityTypeRtId { get; private set; }
    public static Id communicationsCampaignRtId { get; private set; }
    public static Id fundraisingCampaignRtId { get; private set; }
    public static Id bankingTransactionRtId { get; private set; }
    public static Id correctionRtId { get; private set; }
    public static Id alignmentRtId { get; private set; }
    public static Id monthlyAgreementInvoiceRtId { get; private set; }
    public static Id rentalAgreementInvoiceRtId { get; private set; }
    public static Id magazineOrderInvoiceRtId { get; private set; }
    public static Id magazineOrderRtId { get; private set; }
    public static Id campaignOrderRtId { get; private set; }
    
    static {
        List<RecordType> rts = [SELECT Id, DeveloperName, SobjectType FROM RecordType
                                WHERE SobjectType IN :objects];
        
        for (RecordType rt : rts) {
            if (rt.SobjectType == 'Account') {
                if (rt.DeveloperName == 'PersonAccount')
                    personAccountRtId = rt.Id;
                else if (rt.DeveloperName == 'Organisation')
                    organisationRtId = rt.Id;
                else if (rt.DeveloperName == 'ForeignPartner')
                    foreignPartnerRtId = rt.Id;
            } else if (rt.SobjectType == 'Contact') {
                if (rt.DeveloperName == 'Contact')
                    contactRtId = rt.Id;
                else if (rt.DeveloperName == 'EmployeeAbroad')
                    employeeRtId = rt.Id;
            } else if (rt.SobjectType == 'Agreement__c') {
                if (rt.DeveloperName == 'MonthlyDonationAgreement')
                    monthlyAgreementRtId = rt.Id;
                else if (rt.DeveloperName == 'ServiceAgreement')
                    serviceAgreementRtId = rt.Id;
                else if (rt.DeveloperName == 'AnniversaryAgreement')
                    tributeeAgreementRtId = rt.Id;
                else if (rt.DeveloperName == 'CommunityAgreement')
                    communityAgreementRtId = rt.Id;
                else if (rt.DeveloperName == 'SpontaneousAgreement')
                    spontaneousAgreementRtId = rt.Id;
            	else if (rt.DeveloperName == 'RentalAgreement')
            		rentalAgreementRtId = rt.Id;
            } else if (rt.SobjectType == 'Project__c') {
                if (rt.DeveloperName == 'Project')
                    projectRtId = rt.Id;
                else if (rt.DeveloperName == 'ProjectRegion')
                    projectRegionRtId = rt.Id;
                else if (rt.DeveloperName == 'ProjectSLS')
                    projectFelmRtId = rt.Id;
                else if (rt.DeveloperName == 'ActivityType')
                    projectActivityTypeRtId = rt.Id;
            } else if (rt.SobjectType == 'Campaign') {
                if (rt.DeveloperName == 'Communications')
                    communicationsCampaignRtId = rt.Id;
                else if (rt.DeveloperName == 'Fundraising')
                    fundraisingCampaignRtId = rt.Id;
            } else if (rt.SobjectType == 'Payment__c') {
                if (rt.DeveloperName == 'BankingTransaction')
                    bankingTransactionRtId = rt.Id;
                else if (rt.DeveloperName == 'Correction')
                    correctionRtId = rt.Id;
                else if (rt.DeveloperName == 'Alignment')
                    alignmentRtId = rt.Id;
            } else if (rt.SobjectType == 'Invoice__c') {
                if (rt.DeveloperName == 'MonthlyDonationAgreementInvoice')
                    monthlyAgreementInvoiceRtId = rt.Id;
                else if (rt.DeveloperName == 'RentalAgreementInvoice')
                    rentalAgreementInvoiceRtId = rt.Id;    
                else if (rt.DeveloperName == 'MagazineOrderInvoice')
                    magazineOrderInvoiceRtId = rt.Id;
            } else if (rt.SobjectType == 'Order__c') {
                if (rt.DeveloperName == 'MagazineOrder')
                    magazineOrderRtId = rt.Id;
                else if (rt.DeveloperName == 'CampaignOrder')
                    campaignOrderRtId = rt.Id;
            } 
        }
    }
}