public class AlignmentPlanHelper {
    
    // Alignment Plan statuses
    // Comment
    private final static String DRAFT = 'Draft';
    private final static String ACTIVE = 'Active';
    private final static String CLOSED = 'Closed';
    static Integer i;
    {
        Integer i;
    }
    
    public static void switchRecordTypeControlledByStatus(List<Alignment_Plan__c> newAlignmentPlans) {
        Map<String, Id> recordTypeDeveloperNameToIdMap = getRecordTypeDeveloperNameToIdMap();
        
        {
            
        }
        for (Alignment_Plan__c newAlignmentPlan : newAlignmentPlans) {   
            Id recordTypeId = recordTypeDeveloperNameToIdMap.get(newAlignmentPlan.Status__c);
            if (recordTypeId != null) {
                newAlignmentPlan.RecordTypeId = recordTypeId;
            }
        }       
    }
    @AuraEnabled
    public static void updatePercent(Id recordId, Alignment_Plan__c triggerAlignmentPlan) {
        final Id adminsAlignmentPlanId = 'a0d7F000003dAInQAM';
        Set<Id> alignmentPlanIds = new Set<Id>{adminsAlignmentPlanId};
        if(recordId != null)
            alignmentPlanIds.add(recordId);
        
        Map<Id, Alignment_Plan__c> alignmentPlanMap = 
            new Map<Id, Alignment_Plan__c>([SELECT Name, Percent__c FROM Alignment_Plan__c WHERE Id IN :alignmentPlanIds]);
        if (!alignmentPlanMap.containsKey(adminsAlignmentPlanId)) {
            if (Trigger.isExecuting)
                triggerAlignmentPlan.addError('Cannot find Admin Alignment Plan AP-0002');
            else
            	throw new AuraException('Cannot find Admin Alignment Plan AP-0002');
        }
        Alignment_Plan__c alignmentPlanForUpdate;
        if (!Trigger.isExecuting) {
            	
            alignmentPlanForUpdate = alignmentPlanMap.get(recordId);
            alignmentPlanForUpdate.Percent__c = Math.random() * 100;
            update alignmentPlanForUpdate;
        } else {
            triggerAlignmentPlan.Percent__c = Math.random() * 100;
        }
        
    }
    @TestVisible
    private static Map<String, Id> getRecordTypeDeveloperNameToIdMap() {
        Map<String, Id> recordTypeDeveloperNameToIdMap = new Map<String, Id>();
        Map<Id, Schema.RecordTypeInfo> recordTypeInfosMapById = Schema.SObjectType.Alignment_Plan__c.getRecordTypeInfosById();
        for (Id recordTypeId : recordTypeInfosMapById.keySet()) {
            recordTypeDeveloperNameToIdMap.put(recordTypeInfosMapById.get(recordTypeId).getDeveloperName(), recordTypeId);
        }
        return recordTypeDeveloperNameToIdMap;
    }

    public static AlignmentPlanHelper.StatusChange checkChangeOfStatus(Alignment_Plan__c newAlignmentPlan, Alignment_Plan__c oldAlignmentPlan) {
        AlignmentPlanHelper.StatusChange statusChange;
        AlignmentPlanHelper.updatePercent(null, newAlignmentPlan);
        if (oldAlignmentPlan.Status__c == DRAFT && newAlignmentPlan.Status__c == ACTIVE) {
            statusChange = AlignmentPlanHelper.StatusChange.ACTIVATED; 
        } else if (oldAlignmentPlan.Status__c == ACTIVE && newAlignmentPlan.Status__c == CLOSED) {
            statusChange = AlignmentPlanHelper.StatusChange.CLOSED;
        } else {
            statusChange = AlignmentPlanHelper.StatusChange.NOT_CHANGED;
        }
        return statusChange;
    }

    public static void distributeAlignmentPlansOnUpdate(Map<Id, Alignment_Plan__c> newAlignmentPlanMap, Map<Id, Alignment_Plan__c> oldAlignmentPlanMap) {
        Map<Id, Alignment_Plan__c> activatedAlignmentPlanMap = new Map<Id, Alignment_Plan__c>();
        Set<Id> closedAlignmentPlanIds = new Set<Id>();
        for (Alignment_Plan__c newAlignmentPlan : newAlignmentPlanMap.values()) {
            Alignment_Plan__c oldAlignmentPlan = oldAlignmentPlanMap.get(newAlignmentPlan.Id);
            
            AlignmentPlanHelper.StatusChange statusChangeTo  = checkChangeOfStatus(newAlignmentPlan, oldAlignmentPlan);
            if (statusChangeTo == AlignmentPlanHelper.StatusChange.ACTIVATED) {
                activatedAlignmentPlanMap.put(newAlignmentPlan.Id, newAlignmentPlan);
            } else if (statusChangeTo == AlignmentPlanHelper.StatusChange.CLOSED) {
                closedAlignmentPlanIds.add(newAlignmentPlan.Id);
            }
        }
        if (activatedAlignmentPlanMap.size() > 0) {
            //createFundTargets(activatedAlignmentPlanMap);
        }
        if (closedAlignmentPlanIds.size() > 0) {
            //closeFundTargets(closedAlignmentPlanIds);
        }
    }
    
    public enum StatusChange {
        ACTIVATED, 
        CLOSED,
        NOT_CHANGED
    }
        
}
