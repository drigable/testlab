public class PaymentLinkingToolSearchCriteriaWrapper {
	
    @AuraEnabled
	public Boolean isParishUnion { get; set; }
    
    //Payment search criteria
    @AuraEnabled
    public String selectedReferenceType { get; set; }
    @AuraEnabled
	public String sLSbankAccountNumber { get; set; }
    @AuraEnabled
	public Date transactionDateFrom { get; set; }
    @AuraEnabled
	public Date transactionDateTo { get; set; }

    //Account search criteria
    @AuraEnabled
	public String accountName { get; set; }
    @AuraEnabled
	public String addressStreet { get; set; }
    @AuraEnabled
	public String addressCity { get; set; }
    @AuraEnabled
	public String addressPostalCode { get; set; }
    
    //Agreement
    @AuraEnabled
	public String agreementName { get; set; }
    @AuraEnabled
    public String agreementMediaCode { get; set; }
    
    //Fund
    @AuraEnabled
    public String fundName { get; set; }

    //Invoice
    @AuraEnabled
    public String invoiceNumber { get; set; }

    //Campaign
    @AuraEnabled
	public String campaignName { get; set; }
    @AuraEnabled
    public String campaignMediaCode { get; set; }

    //Project
    @AuraEnabled
    public String projectName { get; set; }
    @AuraEnabled
	public String projectCode { get; set; }

}